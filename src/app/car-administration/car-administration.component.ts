import { Component, OnInit, TemplateRef, EventEmitter } from '@angular/core';
import { Car } from '../car.model';
import { CarService } from '../car.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { FormBuilder, FormGroup, Validators, AbstractControl, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-car-administration',
  templateUrl: './car-administration.component.html',
  styleUrls: ['./car-administration.component.css']
})
export class CarAdministrationComponent implements OnInit {

  cars: Car[];
  modalRef: BsModalRef;
  addForm: FormGroup;
  editForm: FormGroup;
  carEdited: Car;

  constructor(private carService: CarService, private modalService: BsModalService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getCars();
  }

  getCars(): void {
      this.carService.getCars().subscribe(cars => this.cars = cars);
  }

  openModal(template: TemplateRef<any>) {
    this.createForm();
    this.modalRef = this.modalService.show(template);
  }

  closeModalAndRefresh() {
    this.getCars();
    this.modalRef.hide();
  }

  createForm() {
    this.addForm = this.formBuilder.group({
      'id': null,
      'mark': '',
      'model': '',
      'description': '',
      'colour': '',
      'productionDate': '',
      'retirementDate': '',
      'mileage': '',
      'serviceStart': '',
      'serviceEnd': ''
    });
    this.editForm = this.formBuilder.group({
      'id': null,
      'mark': '',
      'model': '',
      'description': '',
      'colour': '',
      'productionDate': '',
      'retirementDate': '',
      'mileage': '',
      'serviceStart': '',
      'serviceEnd': ''
    });
  }

  addCar(post) {
    const car = {
      id: post.id,
      mark: post.mark,
      model: post.model,
      description: post.description,
      colour: post.colour,
      productionDate: post.productionDate,
      retirementDate: post.retirementDate,
      mileage: post.mileage,
      serviceStart: post.serviceStart,
      serviceEnd: post.serviceEnd
    } as Car;
    this.carService.add(car).subscribe(car => this.cars.push(car));
  }

  carToEdit(car: Car) {
    this.carEdited = car;
  }

  editCar(post) {
    this.carEdited.mark = post.mark || this.carEdited.mark;
    this.carEdited.model = post.model || this.carEdited.model;
    this.carEdited.description = post.description || this.carEdited.description;
    this.carEdited.colour = post.colour || this.carEdited.colour;
    this.carEdited.productionDate = post.productionDate || this.carEdited.productionDate;
    this.carEdited.retirementDate = post.retirementDate || this.carEdited.retirementDate;
    this.carEdited.mileage = post.mileage || this.carEdited.mileage;
    this.carEdited.serviceStart = post.serviceStart || this.carEdited.serviceStart;
    this.carEdited.serviceEnd = post.serviceEnd || this.carEdited.serviceEnd;
    this.carService.update(this.carEdited).subscribe();
  }

  deleteCar(car: Car): void {
    this.cars = this.cars.filter(c => c !== car);
    this.carService.delete(car).subscribe();
  }
}
