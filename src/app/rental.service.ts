import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Customer } from "./customer.model";
import { Observable } from 'rxjs/Observable';
import { Rental } from './rental.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class RentalService {
  
  private rentalsUrl = 'api/rentals';

  constructor(private http: HttpClient) { }

  getRentals (): Observable<Rental[]> {
    return this.http.get<Rental[]>(this.rentalsUrl);
  }

  getRental(id: number): Observable<Rental> {
    const url = `${this.rentalsUrl}/${id}`;
    return this.http.get<Rental>(url);
  }

  add(rental: Rental): Observable<Rental> {
    return this.http.post<Rental>(this.rentalsUrl, rental, httpOptions);
  }

  update(rental: Rental): Observable<Rental> {
    return this.http.put<Rental>(this.rentalsUrl, rental, httpOptions);
  }

  delete(rental: Rental): Observable<Rental> {
    const id = typeof rental === 'number' ? rental : rental.id;
    const url = `${this.rentalsUrl}/${id}`;
    return this.http.delete<Rental>(url);
  }

}