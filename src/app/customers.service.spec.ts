import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { HttpClient, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Customer } from "./customer.model";
import { CustomerService } from "./customers.service";
import { asyncData } from '../testing/async-observable-helpers';


describe ('CustomersService (with spies)', () => {
  let httpClientSpy: { get: jasmine.Spy };
  let customerService: CustomerService;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    customerService = new CustomerService(<any> httpClientSpy);
  });

  it('should return expected customers (HttpClient called once)', () => {
    const expectedCustomers: Customer[] =
      [{ id: 1, firstName: 'Krzysztof', lastName: 'Ibisz', email: "ibisz@interia.pl" }];

    httpClientSpy.get.and.returnValue(asyncData(expectedCustomers));

    customerService.getCustomers().subscribe(
      customers => expect(customers).toEqual(expectedCustomers, 'expected customers'),
      fail
    );
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  });

});

describe('CustomerService (with mocks)', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let customerService: CustomerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [ CustomerService ]
    });

    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    customerService = TestBed.get(CustomerService);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  describe('#getCustomers', () => {
    let expectedCustomers: Customer[];

    beforeEach(() => {
      customerService = TestBed.get(CustomerService);
       expectedCustomers = [
        { id: 1, firstName: 'Krzysztof', lastName: 'Ibisz', email: "ibisz@interia.pl" },
        { id: 2, firstName: 'Zuzanna', lastName: 'Potocka', email: "potocka@gmail.com" },
        { id: 3, firstName: 'Mariusz', lastName: 'Kowalski', email: "kowalski@gmail.com" },
        { id: 4, firstName: 'Marietta', lastName: 'Zukowska', email: "zukowska@gmail.com" }
       ] as Customer[];
    });

    it('should return expected customers (called once)', () => {
      customerService.getCustomers().subscribe(
        customers => expect(customers).toEqual(expectedCustomers, 'should return expected customers'),
        fail
      );

      const req = httpTestingController.expectOne(customerService.customersUrl);
      expect(req.request.method).toEqual('GET');

      req.flush(expectedCustomers);
    });

    it('should be OK returning no customers', () => {
      customerService.getCustomers().subscribe(
        customers => expect(customers.length).toEqual(0, 'should have empty customers array'),
        fail
      );

      const req = httpTestingController.expectOne(customerService.customersUrl);
      req.flush([]);
    });

    it('should return expected customers (called multiple times)', () => {
      customerService.getCustomers().subscribe();
      customerService.getCustomers().subscribe();
      customerService.getCustomers().subscribe(
        customers => expect(customers).toEqual(expectedCustomers, 'should return expected heroes'),
        fail
      );

      const requests = httpTestingController.match(customerService.customersUrl);
      expect(requests.length).toEqual(3, 'calls to getCustomers()');

      requests[0].flush([]);
      requests[1].flush([{ id: 1, firstName: 'Krzysztof', lastName: 'Ibisz', email: "ibisz@interia.pl" }]);
      requests[2].flush(expectedCustomers);
    });
  });

  describe('#getCustomer', () => {
    
    const makeUrl = (id: number) => `${customerService.customersUrl}/${id}`;
    
    it('should get a customer and return it', () => {

      const expectedCustomer: Customer = { id: 1, firstName: 'Krzysztof', lastName: 'Ibisz', email: "ibisz@interia.pl" };

      customerService.getCustomer(expectedCustomer.id).subscribe(
        data => expect(data).toEqual(expectedCustomer, 'should return the customer')
      );

      const req = httpTestingController.expectOne(makeUrl(expectedCustomer.id));
      expect(req.request.method).toEqual('GET');

      const expectedResponse = new HttpResponse(
        { status: 200, statusText: 'OK', body: expectedCustomer });
      req.event(expectedResponse);
    });

   });

  describe('#addCustomer', () => {
    
    const makeUrl = (id: number) => `${customerService.customersUrl}/?id=${id}`;

    it('should add a customer and return it', () => {

      const addCustomer: Customer = { id: 1, firstName: 'Krzysztof', lastName: 'Ibisz', email: "ibisz@interia.pl" };

      customerService.add(addCustomer).subscribe(
        data => expect(data).toEqual(addCustomer, 'should return the customer'),
        fail
      );

      const req = httpTestingController.expectOne(customerService.customersUrl);
      expect(req.request.method).toEqual('POST');
      expect(req.request.body).toEqual(addCustomer);

      const expectedResponse = new HttpResponse(
        { status: 200, statusText: 'OK', body: addCustomer });
      req.event(expectedResponse);
    });

  });

  describe('#updateCustomer', () => {
    
    const makeUrl = (id: number) => `${customerService.customersUrl}/?id=${id}`;

    it('should update a customer and return it', () => {

      const updateCustomer: Customer = { id: 1, firstName: 'Krzysztof', lastName: 'Ibisz', email: "ibisz@interia.pl" };

      customerService.update(updateCustomer).subscribe(
        data => expect(data).toEqual(updateCustomer, 'should return the customer'),
        fail
      );

      const req = httpTestingController.expectOne(customerService.customersUrl);
      expect(req.request.method).toEqual('PUT');
      expect(req.request.body).toEqual(updateCustomer);

      const expectedResponse = new HttpResponse(
        { status: 200, statusText: 'OK', body: updateCustomer });
      req.event(expectedResponse);
    });

   });

   describe('#deleteCustomer', () => {
    
    const makeUrl = (id: number) => `${customerService.customersUrl}/${id}`;
    
    it('should delete a customer and return it', () => {

      const deleteCustomer: Customer = { id: 1, firstName: 'Krzysztof', lastName: 'Ibisz', email: "ibisz@interia.pl" };

      customerService.delete(deleteCustomer).subscribe(
        data => expect(data).toEqual(deleteCustomer, 'should return the customer'),
        fail
      );

      const req = httpTestingController.expectOne(makeUrl(deleteCustomer.id));
      expect(req.request.method).toEqual('DELETE');
      expect(req.request.body).toEqual(null);

      const expectedResponse = new HttpResponse(
        { status: 200, statusText: 'OK', body: deleteCustomer });
      req.event(expectedResponse);
    });

   });
});
