import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminComponent } from './admin.component';
import { UserService } from '../user.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Component, Directive, Input } from '@angular/core';
import { By } from '@angular/platform-browser';

@Component({selector: 'app-header', template: ''})
class AppHeaderStubComponent { }

@Component({selector: 'router-outlet', template: ''})
class RouterOutletStubComponent { }

@Directive({
  selector: '[routerLink]',
  host: { '(click)': 'onClick()' }
})
class RouterLinkDirectiveStub {
  @Input('routerLink') linkParams: any;
  navigatedTo: any = null;

  onClick() {
    this.navigatedTo = this.linkParams;
  }
}

describe('AdminComponent', () => {
  let component: AdminComponent;
  let fixture: ComponentFixture<AdminComponent>;
  const userServiceSpy = jasmine.createSpyObj('UserService', ['getUserName']);
  const stubUser = { id: 1, userName: "user", password: "user", userRole: "user", customerId: 1};
  let linkDes: any;
  let routerLinks: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ ReactiveFormsModule, FormsModule ],
      declarations: [ 
        AdminComponent, 
        AppHeaderStubComponent, 
        RouterOutletStubComponent, 
        RouterLinkDirectiveStub 
      ],
      providers: [ 
        { provide: UserService, useValue: userServiceSpy }
      ]
      })
    .compileComponents();
    userServiceSpy.getUserName.and.returnValue(stubUser.userName);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    linkDes = fixture.debugElement.queryAll(By.directive(RouterLinkDirectiveStub));
    routerLinks = linkDes.map(de => de.injector.get(RouterLinkDirectiveStub));
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should get username admin', () => {
    component.ngOnInit();
    expect(component.username).toBe(stubUser.userName);
  });

  it('should get routerLinks', () => {
    expect(routerLinks.length).toBe(3, 'should have 3 routerLinks');
    expect(routerLinks[0].linkParams).toBe('/admin/customer-management');
    expect(routerLinks[1].linkParams).toBe('/admin/car-administration');
    expect(routerLinks[2].linkParams).toBe('/admin/rental-administration');
  });
  
  it('should click customer management link in admin', () => {
    const userLinkDe = linkDes[0];
    const userLink = routerLinks[0];
  
    expect(userLink.navigatedTo).toBeNull('should not have navigated yet');
  
    userLinkDe.triggerEventHandler('click', null);
    fixture.detectChanges();
  
    expect(userLink.navigatedTo).toBe('/admin/customer-management');
  });
});
