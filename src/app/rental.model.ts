export class Rental {
    id: number;
    car_id: number;
    customer_id: number;
    status: string;
    remarks: string;
    dateFrom: Date;
    dateTo: Date;
    hasGps: boolean;
    hasWifi: boolean;
}