import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Car } from './car.model';

export class InMemoryDataService implements InMemoryDbService {
                   
  createDb() {
    
    const customers = [
        { id: 1, firstName: 'Krzysztof', lastName: 'Ibisz', email: "ibisz@interia.pl" },
        { id: 2, firstName: 'Zuzanna', lastName: 'Potocka', email: "potocka@gmail.com" },
        { id: 3, firstName: 'Mariusz', lastName: 'Kowalski', email: "kowalski@gmail.com" },
        { id: 4, firstName: 'Marietta', lastName: 'Zukowska', email: "zukowska@gmail.com" },
        { id: 5, firstName: 'Anna', lastName: 'Jantar', email: "anna.jantar@gmail.com" },
        { id: 6, firstName: 'Hubert', lastName: 'Urbanski', email: "urbanski@gmail.com" },
        { id: 7, firstName: 'Katarzyna', lastName: 'Lewandowska', email: "lewandowska@gmail.com" },
        { id: 8, firstName: 'Filip', lastName: 'Nowak', email: "filip.nowak@gmail.com" },
        { id: 9, firstName: 'Karolina', lastName: 'Zlotopolska', email: "zlotopolska@gmail.com" },
        { id: 10, firstName: 'Doriad', lastName: 'Duda', email: "duda@gmail.com" }
    ];

    const rentals = [
      { id: 1, car_id: 1, customer_id: 1, status: 'reserved', dateFrom: new Date(new Date('2018-01-01')), dateTo: new Date('2018-01-10'), hasGps: true, hasWifi: false },
      { id: 2, car_id: 2, customer_id: 2, status: 'reserved', dateFrom: new Date('2018-01-01'), dateTo: new Date('2018-01-10'), hasGps: false, hasWifi: true },
      { id: 3, car_id: 3, customer_id: 3, status: 'returned', dateFrom: new Date('2018-01-01'), dateTo: new Date('2018-01-10'), hasGps: false, hasWifi: true },
      { id: 4, car_id: 4, customer_id: 4, status: 'returned', dateFrom: new Date('2018-01-01'), dateTo: new Date('2018-01-10'), hasGps: true, hasWifi: false },
      { id: 5, car_id: 1, customer_id: 1, status: 'reserved', dateFrom: new Date('2018-01-01'), dateTo: new Date('2018-01-10'), hasGps: false, hasWifi: true },
      { id: 6, car_id: 2, customer_id: 2, status: 'rented', dateFrom: new Date('2018-01-01'), dateTo: new Date('2018-01-10'), hasGps: true, hasWifi: true },
      { id: 7, car_id: 3, customer_id: 3, status: 'rented', dateFrom: new Date('2018-01-01'), dateTo: new Date('2018-01-10'), hasGps: false, hasWifi: false },
      { id: 8, car_id: 4, customer_id: 4, status: 'rented', dateFrom: new Date('2018-01-01'), dateTo: new Date('2018-01-10'), hasGps: true, hasWifi: false }
    ];

    const cars = [
        {id: 1, mark: "Opel", model: "Astra", description: "ideal for family", colour: 'red', productionDate: new Date('2011-01-01'), retirementDate: new Date('2016-01-01'), mileage: 150000, serviceStart: new Date('2018-02-01'), serviceEnd: new Date('2018-02-05')},
        {id: 2, mark: "Toyota", model: "Yaris", description: "good for city", colour: 'black', productionDate: new Date('2015-01-01'), retirementDate: new Date('2020-01-01'), mileage: 100000, serviceStart: new Date('2018-02-01'), serviceEnd: new Date('2018-02-05')},
        {id: 3, mark: "Audi", model: "A5", description: "fast, pretty and reliable", colour: 'white', productionDate: new Date('2016-01-01'), retirementDate: new Date('2021-01-01'), mileage: 140000, serviceStart: new Date('2018-02-01'), serviceEnd: new Date('2018-02-05')},
        {id: 4, mark: "Peugeot", model: "307", description: "ideal for family", colour: 'purple', productionDate: new Date('2014-01-01'), retirementDate: new Date('2019-01-01'), mileage: 130000, serviceStart: new Date('2018-02-01'), serviceEnd: new Date('2018-02-05')},
        {id: 5, mark: "BMW", model: "i8", description: "super fast", colour: 'green', productionDate: new Date('2013-01-01'), retirementDate: new Date('2018-01-01'), mileage: 100000, serviceStart: new Date('2018-02-01'), serviceEnd: new Date('2018-02-05')},
        {id: 6, mark: "Opel", model: "Adam", description: "good for city", colour: 'blue', productionDate: new Date('2012-01-01'), retirementDate: new Date('2017-01-01'), mileage: 110000, serviceStart: new Date('2018-02-01'), serviceEnd: new Date('2018-02-05')},
        {id: 7, mark: "Honda", model: "Civic", description: "fast and reliable", colour: 'black', productionDate: new Date('2017-01-01'), retirementDate: new Date('2022-01-01'), mileage: 120000, serviceStart: new Date('2018-02-01'), serviceEnd: new Date('2018-02-05')}
    ];

    const users = [
      { id: 1, userName: "user", password: "user", userRole: "user", customerId: 1},
      { id: 2, userName: "user", password: "user", userRole: "user", customerId: 2},
      { id: 3, userName: "user", password: "user", userRole: "user", customerId: 3},
      { id: 4, userName: "admin", password: "admin", userRole: "admin", customerId: null},
      { id: 4, userName: "admin", password: "admin", userRole: "admin", customerId: null}
    ];
    
    return {customers, rentals, cars, users};
  }
}