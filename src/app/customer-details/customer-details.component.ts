import { Component, OnInit, Input, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { FormBuilder, FormGroup, Validators, AbstractControl, ReactiveFormsModule } from '@angular/forms';
import { Customer } from '../customer.model';
import { CustomerService } from '../customers.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.css']
})
export class CustomerDetailsComponent implements OnInit {

  @Input()
  customer: Customer;

  editForm: FormGroup;
  modalRef: BsModalRef;

  constructor(private customerService: CustomerService, private modalService: BsModalService,
    private formBuilder: FormBuilder) {
      this.createForm();
  }

  ngOnInit() {
  }

  openModal(template: TemplateRef<any>) {
    this.createForm();
    this.modalRef = this.modalService.show(template);
  }

  closeModalAndRefresh() {
    this.modalRef.hide();
  }

  createForm() {
    this.editForm = this.formBuilder.group({
      'firstName': '',
      'lastName': '',
      'email': ''
    });
  }

  editCustomer(post) {
    const editedCustomer = {
      id: this.customer.id,
      firstName: post.firstName || this.customer.firstName,
      lastName: post.lastName || this.customer.lastName,
      email: post.email || this.customer.email
    } as Customer; 
    this.customerService.update(editedCustomer).subscribe();
  }

  deleteCustomer(customer: Customer): void {
    this.customerService.delete(customer).subscribe();
  }
}
