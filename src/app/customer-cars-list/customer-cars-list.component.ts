import { Component, OnInit, Input, OnChanges, SimpleChanges, SimpleChange } from '@angular/core';
import { Customer } from '../customer.model';
import { Car } from '../car.model';
import { CarService } from '../car.service';
import { Rental } from '../rental.model';
import { RentalService } from '../rental.service';
import { ChangeDetectionStrategy } from '@angular/compiler/src/core';

@Component({
  selector: 'app-customer-cars-list',
  templateUrl: './customer-cars-list.component.html',
  styleUrls: ['./customer-cars-list.component.css'],
})
export class CustomerCarsListComponent implements OnInit {

  @Input()
  customer: Customer;

  @Input()
  cars: Car[] = [];
  
  rentals: Rental[];

  constructor(private carService: CarService, private rentalService: RentalService) { }

  ngOnInit() {
    this.getRentals();
  }

  getRentals(): void {
    this.rentalService.getRentals().subscribe(rentals => this.rentals = rentals);
  }
}
