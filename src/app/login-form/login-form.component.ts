import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']  
})
export class LoginFormComponent implements OnInit {

  username: string;
  password: string;
  constructor(private router: Router, private userService: UserService) { }

  ngOnInit() {
  }

  login(event) {
    this.username= event.target.elements[0].value;
    this.password = event.target.elements[1].value;
    
    if(this.username == 'admin' && this.password == 'admin') {
      this.userService.setUserLoggedIn(this.username);
      this.router.navigate(['admin/customer-management']);
    }
    if(this.username == 'user' && this.password == 'user') {
      this.userService.setUserLoggedIn(this.username);
      this.router.navigate(['user/car-search-management']);
    }
  }
}
