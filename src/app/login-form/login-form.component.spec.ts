import { LoginFormComponent } from './login-form.component';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators, ControlContainer } from '@angular/forms';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';


describe('LoginFormComponent', () => {
  let component: LoginFormComponent;
  let fixture: ComponentFixture<LoginFormComponent>;
  const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);
  const userServiceSpy = jasmine.createSpyObj('UserService', ['getUsers']);
  
  const stubUsers = [
    { id: 1, userName: "user", password: "user", userRole: "user", customerId: 1},
    { id: 2, userName: "user", password: "user", userRole: "user", customerId: 2},
    { id: 3, userName: "user", password: "user", userRole: "user", customerId: 3},
    { id: 4, userName: "admin", password: "admin", userRole: "admin", customerId: null},
    { id: 4, userName: "admin", password: "admin", userRole: "admin", customerId: null}
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
    imports: [ ReactiveFormsModule, FormsModule ],
    declarations: [ LoginFormComponent ],
    providers: [ 
      { provide: UserService, useValue: userServiceSpy },
      { provide: Router, useValue: routerSpy },
    ]
    })
    .compileComponents();
    userServiceSpy.getUsers.and.returnValue(stubUsers)
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should have no login data defined', () => {
    expect(component.username).toBeUndefined();
    expect(component.password).toBeUndefined();
  });


})