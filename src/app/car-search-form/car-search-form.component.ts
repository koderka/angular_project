import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl, ReactiveFormsModule } from '@angular/forms';
import { CarService } from '../car.service';
import { Car } from '../car.model';
import { RentalService } from '../rental.service';
import { Rental } from '../rental.model';


@Component({
  selector: 'app-car-search-form',
  templateUrl: './car-search-form.component.html',
  styleUrls: ['./car-search-form.component.css']
})
export class CarSearchFormComponent implements OnInit {

  @Output()
  foundCars = new EventEmitter<Car[]>();

  cars: Car[];
  rentals: Rental[];
  searchForm: FormGroup;

  constructor(private carService: CarService, private rentalService: RentalService, private formBuilder: FormBuilder) {
    this.createForm();
  }

  ngOnInit() {
    this.getRentals();
    this.getCars();
  }

  getCars(): void {
    this.carService.getCars().subscribe(cars => this.cars = cars);
  }
  
  getRentals(): void {
    this.rentalService.getRentals().subscribe(rentals => this.rentals = rentals);
  }

  isDateRangeOverlapping(searchFrom: Date, searchTo: Date, checkFrom: Date, checkTo: Date): boolean {
    return checkFrom < searchFrom && checkTo > searchTo ||
           checkFrom > searchFrom && checkFrom < searchTo ||
           checkTo > searchFrom && checkTo < searchTo;
  }

  createForm() {
    this.searchForm = this.formBuilder.group({
      'rentDate': null,
      'returnDate': null
    })
  }
  searchCars(post) {
    this.createForm();
    this.getRentals();
    this.getCars();
    this.rentals = this.rentals.filter(r => this.isDateRangeOverlapping(post.rentDate, post.returnDate, r.dateFrom, r.dateTo));
    this.cars = this.cars.filter(c => !this.isDateRangeOverlapping(post.rentDate, post.returnDate, c.serviceStart, c.serviceEnd) &&
      this.rentals.filter(r => r.car_id === c.id).length === 0 && c.retirementDate > post.returnDate);
      return this.foundCars.emit(this.cars);
  }
  
}
