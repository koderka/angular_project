import { Injectable } from '@angular/core';
import { User } from './user.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Rental } from './rental.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class UserService {

  private usersUrl = 'api/users';

  private isUserLoggedIn;
  public userName: string;

  constructor(private http: HttpClient) { 
  	this.isUserLoggedIn = false;
  }

  setUserLoggedIn(userName: string) {
  	this.isUserLoggedIn = true;
    this.userName = userName;
  }

  getUserLoggedIn() {
  	return this.isUserLoggedIn;
  }

  getUserName() {
    return this.userName;
  }

  getUsers (): Observable<User[]> {
    return this.http.get<User[]>(this.usersUrl);
  }

  getUser(id: number): Observable<User> {
    const url = `${this.usersUrl}/${id}`;
    return this.http.get<User>(url);
  }

}