export class User {
    id: number;
    userName: string;
    password: string;
    userRole: string;
    customerId: number;
}