import { Component, OnInit, TemplateRef } from '@angular/core';
import { Rental } from '../rental.model';
import { RentalService } from '../rental.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { FormBuilder, FormGroup, Validators, AbstractControl, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-rental-administration',
  templateUrl: './rental-administration.component.html',
  styleUrls: ['./rental-administration.component.css']
})
export class RentalAdministrationComponent implements OnInit {

  rentals: Rental[];
  modalRef: BsModalRef;
  addForm: FormGroup;
  rentForm: FormGroup;
  returnForm: FormGroup;
  rentalEdited: Rental;
  checkHasGps: boolean = false;
  checkHasWifi: boolean = false;

  constructor(private rentalService: RentalService, private modalService: BsModalService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getRentals();
  }

  getRentals(): void {
    this.rentalService.getRentals().subscribe(rentals => this.rentals = rentals);
  }

  openModal(template: TemplateRef<any>) {
    this.createForm();
    this.modalRef = this.modalService.show(template);
  }

  closeModalAndRefresh() {
    this.getRentals();
    this.modalRef.hide();
  }

  createForm() {
    this.addForm = this.formBuilder.group({
      'id': null,
      'car_id': '',
      'customer_id': '',
      'status': '',
      'dateFrom': '',
      'dateTo': '',
      'hasGps': false,
      'hasWifi': false
    });

    this.rentForm = this.formBuilder.group({
      'dateFrom': '',
      'dateTo': '',
      'hasGps': false,
      'hasWifi': false
    })

    this.returnForm = this.formBuilder.group({
      'dateFrom': '',
      'dateTo': '',
      'remarks' : ''
    })
  }

  changeValue(event) {
    const checked: boolean = event.target.checked;
  }

  addRental(post) {
    const rental = {
      id: post.id,
      car_id: post.car_id,
      customer_id: post.customer_id,
      status: post.status,
      dateFrom: post.dateFrom,
      dateTo: post.dateTo,
      hasGps: post.hasGps,
      hasWifi: post.hasWifi
    } as Rental;
    this.rentalService.add(rental).subscribe(rental => this.rentals.push(rental));
  }

  rentalToEdit(rental: Rental) {
    this.rentalEdited = rental;
  }

  rentCar(post) {
    this.rentalEdited.status = "rented";
    this.rentalEdited.dateFrom = post.dateFrom;
    this.rentalEdited.dateTo = post.dateTo;
    this.rentalEdited.hasGps = post.hasGps;
    this.rentalEdited.hasWifi = post.hasWifi;
    this.rentalService.update(this.rentalEdited).subscribe();
  }

  returnCar(post) {
    this.rentalEdited.status = "returned";
    this.rentalEdited.dateFrom = post.dateFrom;
    this.rentalEdited.dateTo = post.dateTo;
    this.rentalEdited.remarks = post.remarks;
    this.rentalService.update(this.rentalEdited).subscribe();
  }

  deleteRental(rental: Rental): void {
    this.rentals = this.rentals.filter(r => r !== rental);
    this.rentalService.delete(rental).subscribe();
  }
}
