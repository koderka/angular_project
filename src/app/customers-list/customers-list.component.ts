import { Component, OnInit, Output, EventEmitter, TemplateRef } from '@angular/core';
import { Customer } from '../customer.model';
import { CustomerService } from '../customers.service';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Car } from '../car.model';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-customers-list',
  templateUrl: './customers-list.component.html',
  styleUrls: ['./customers-list.component.css']
})
export class CustomersListComponent implements OnInit {
  
  customers: Customer[];

  @Output()
  customerSelected = new EventEmitter<Customer>();

  @Output()
  carsOfCustomerSelected = new EventEmitter<Customer>();

  customerForm: FormGroup;
  modalRef: BsModalRef;
  
  constructor(private customerService: CustomerService, private modalService: BsModalService,
  private formBuilder: FormBuilder) { 
    this.createForm();
  }

  ngOnInit() {
    this.getCustomers();
  }

  customerClicked(customer: Customer) {
    this.customerSelected.emit(customer);
    this.carsOfCustomerSelected.emit(customer);
  }

  openModal(template: TemplateRef<any>) {
    this.createForm();
    this.modalRef = this.modalService.show(template);
  }

  closeModalAndRefresh() {
    this.getCustomers();
    this.modalRef.hide();
  }

  getCustomers(): void {
    this.customerService.getCustomers()
    .subscribe(customers => this.customers = customers);
  }

  createForm() {
    this.customerForm = this.formBuilder.group({
      'id': null,
      'firstName': '',
      'lastName': '',
      'email': ''
    });
  }

  addCustomer(post) {
    const customer = {
      id: post.id,
      firstName: post.firstName,
      lastName: post.lastName,
      email: post.email
    } as Customer;
    this.customerService.add(customer).subscribe(customer => this.customers.push(customer));
  }

}
