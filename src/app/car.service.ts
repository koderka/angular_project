import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Customer } from "./customer.model";
import { Observable } from 'rxjs/Observable';
import { Car } from './car.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class CarService {
  
  private carsUrl = 'api/cars';

  constructor(private http: HttpClient) { }

  getCars (): Observable<Car[]> {
    return this.http.get<Car[]>(this.carsUrl);
  }

  getCar(id: number): Observable<Car> {
    const url = `${this.carsUrl}/${id}`;
    return this.http.get<Car>(url);
  }

  add(car: Car): Observable<Car> {
    return this.http.post<Car>(this.carsUrl, car, httpOptions);
  }

  update(car: Car): Observable<Car> {
    return this.http.put<Car>(this.carsUrl, car, httpOptions);
  }

  delete(car: Car): Observable<Car> {
    const id = typeof car === 'number' ? car : car.id;
    const url = `${this.carsUrl}/${id}`;
    return this.http.delete<Car>(url);
  }

}