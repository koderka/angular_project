import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WelcomeComponent } from './welcome.component';
import { Component } from '@angular/core';

@Component({selector: 'app-login-form', template: ''})
class AppLoginFormStubComponent { }

@Component({selector: 'app-background-image', template: ''})
class AppBackgroundImageStubComponent { }

describe('WelcomeComponent', () => {
  let component: WelcomeComponent;
  let fixture: ComponentFixture<WelcomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WelcomeComponent, AppLoginFormStubComponent, AppBackgroundImageStubComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WelcomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should contain welcome title', () => {
    const h1: HTMLElement = fixture.debugElement.nativeElement.querySelector('h1');
    expect(h1.textContent).toContain('Welcome in Rental Cars!');
  });
});
