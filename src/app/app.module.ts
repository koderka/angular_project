import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';
import { RouterModule, Routes} from '@angular/router';

import { AppComponent } from './app.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { HeaderComponent } from './header/header.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { BackgroundImageComponent } from './background-image/background-image.component';
import { AdminComponent } from './admin/admin.component';
import { CustomerManagementComponent } from './customer-management/customer-management.component';
import { CustomersListComponent } from './customers-list/customers-list.component';
import { CustomerDetailsComponent } from './customer-details/customer-details.component';
import { CustomerCarsListComponent } from './customer-cars-list/customer-cars-list.component';
import { CarAdministrationComponent } from './car-administration/car-administration.component';
import { RentalAdministrationComponent } from './rental-administration/rental-administration.component';
import { UserComponent } from './user/user.component';
import { CarSearchManagementComponent } from './car-search-management/car-search-management.component';
import { CarSearchFormComponent } from './car-search-form/car-search-form.component';
import { CarListComponent } from './car-list/car-list.component';
import { ReservationManagementComponent } from './reservation-management/reservation-management.component';
import { UserService } from './user.service';
import { CustomerService } from './customers.service';
import { CarService } from './car.service';
import { RentalService } from './rental.service';
import { NotFoundComponent } from './not-found/not-found.component';
import { FilterPipe } from './filter.pipe';

import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { Ng2OrderModule } from 'ng2-order-pipe';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ReservationsListComponent } from './reservations-list/reservations-list.component';

const appRoutes: Routes = [
  { path: '', component: WelcomeComponent },
  { path: 'admin', component: AdminComponent,
    children: [
    { path: 'customer-management', component: CustomerManagementComponent },
    { path: 'car-administration', component: CarAdministrationComponent  },
    { path: 'rental-administration', component: RentalAdministrationComponent }
  ]},
  { path: 'user', component: UserComponent, 
    children: [
      { path: 'car-search-management', component: CarSearchManagementComponent},
      { path: 'reservations', component: ReservationManagementComponent }
    ]},
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    HeaderComponent,
    LoginFormComponent,
    BackgroundImageComponent,
    AdminComponent,
    CustomerDetailsComponent,
    CustomerManagementComponent,
    CustomersListComponent,
    CustomerCarsListComponent,
    CarAdministrationComponent,
    RentalAdministrationComponent,
    UserComponent,
    CarSearchManagementComponent,
    CarSearchFormComponent,
    CarListComponent,
    ReservationManagementComponent,
    FilterPipe,
    NotFoundComponent,
    ReservationsListComponent

  ],
  imports: [
    BrowserModule, 
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    ),
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    TimepickerModule.forRoot(),
    Ng2OrderModule
  ],
  providers: [UserService, CustomerService, CarService, RentalService],
  bootstrap: [AppComponent, AdminComponent, UserComponent]
})
export class AppModule { }
