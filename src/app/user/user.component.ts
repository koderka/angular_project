import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Car } from '../car.model';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
