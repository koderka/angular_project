import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserComponent } from './user.component';
import { Component, Directive, Input } from '@angular/core';
import { By } from '@angular/platform-browser';

@Component({selector: 'app-header', template: ''})
class AppHeaderStubComponent { }

@Component({selector: 'router-outlet', template: ''})
class RouterOutletStubComponent { }

@Directive({
  selector: '[routerLink]',
  host: { '(click)': 'onClick()' }
})
class RouterLinkDirectiveStub {
  @Input('routerLink') linkParams: any;
  navigatedTo: any = null;

  onClick() {
    this.navigatedTo = this.linkParams;
  }
}

describe('UserComponent', () => {
  let component: UserComponent;
  let fixture: ComponentFixture<UserComponent>;
  let linkDes: any;
  let routerLinks: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        UserComponent, 
        AppHeaderStubComponent, 
        RouterOutletStubComponent, 
        RouterLinkDirectiveStub ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    linkDes = fixture.debugElement.queryAll(By.directive(RouterLinkDirectiveStub));
    routerLinks = linkDes.map(de => de.injector.get(RouterLinkDirectiveStub));
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should get routerLinks', () => {
    expect(routerLinks.length).toBe(2, 'should have 2 routerLinks');
    expect(routerLinks[0].linkParams).toBe('/user/car-search-management');
    expect(routerLinks[1].linkParams).toBe('/user/reservations');
  });
  
  it('should click car search link in user', () => {
    const userLinkDe = linkDes[0];
    const userLink = routerLinks[0];
  
    expect(userLink.navigatedTo).toBeNull('should not have navigated yet');
  
    userLinkDe.triggerEventHandler('click', null);
    fixture.detectChanges();
  
    expect(userLink.navigatedTo).toBe('/user/car-search-management');
  });
});
