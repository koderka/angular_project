import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customers.service';
import { Customer } from '../customer.model';
import { CarService } from '../car.service';
import { RentalService } from '../rental.service';
import { Car } from '../car.model';
import { Rental } from '../rental.model';
import { CarListComponent } from '../car-list/car-list.component';

@Component({
  selector: 'app-customer-management',
  templateUrl: './customer-management.component.html',
  styleUrls: ['./customer-management.component.css'],
  providers: [ CustomerService ]
})
export class CustomerManagementComponent implements OnInit {

  car: Car;
  currentCustomer: Customer;
  currentCustomerCars: Car[] = [];
  rentals: Rental[];

  constructor(private carService: CarService, private rentalService: RentalService) { }

  ngOnInit() {
    this.getRentals();
  }

  customerSelected(customer: Customer) {
    this.currentCustomer = customer;
  }
  
  carsOfCustomerSelected(customer: Customer) {
    this.getRentals();
      this.rentals = this.rentals.filter(r => r.customer_id === customer.id);
      this.currentCustomerCars = [];
      this.rentals.forEach(rental =>{
        this.getCar(rental.car_id);
        this.currentCustomerCars.push(this.car);
      });
    return this.currentCustomerCars;
  }

  getCars(): void {
    this.carService.getCars().subscribe(cars => this.currentCustomerCars = cars);
  }

  getCar(id: number): void {
    this.carService.getCar(id).subscribe(car => this.car = car);
  }

  getRentals(): void {
    this.rentalService.getRentals().subscribe(rentals => this.rentals = rentals);
  }
}
