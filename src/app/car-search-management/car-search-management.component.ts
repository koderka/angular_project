import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { UserService } from '../user.service';
import { Car } from '../car.model';

@Component({
  selector: 'app-car-search-management',
  templateUrl: './car-search-management.component.html',
  styleUrls: ['./car-search-management.component.css']
})
export class CarSearchManagementComponent implements OnInit {

  constructor(private user: UserService) { }

  carsFound: Car[];

  ngOnInit() {
  }

  foundCars(cars: Car[]) {
    this.carsFound = cars;
  }

}
