import { Component, OnInit, Input, TemplateRef } from '@angular/core';
import { CarService } from '../car.service';
import { Car } from '../car.model';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { RentalService } from '../rental.service';
import { Rental } from '../rental.model';
import { UserService } from '../user.service';
import { User } from '../user.model';

@Component({
  selector: 'app-car-list',
  templateUrl: './car-list.component.html',
  styleUrls: ['./car-list.component.css']
})
export class CarListComponent implements OnInit {

  @Input()
  cars: Car[];
  key: string = 'mark';
  reverse: boolean = false;
  modalRef: BsModalRef;
  bookForm: FormGroup;
  rentals: Rental[];
  users: User[];
  bookedCar: Car;
  checkHasGps: boolean = false;
  checkHasWifi: boolean = false;
  userLoggedIn: string;

  constructor(private carService: CarService, private userService: UserService, private rentalService: RentalService, private modalService: BsModalService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getUsers();
    this.getRentals();
    this.createForm();
  }

  sort(){
  }

  openModal(template: TemplateRef<any>) {
    this.createForm();
    this.checkHasGps = false;
    this.checkHasWifi = false;
    this.modalRef = this.modalService.show(template);
  }

  closeModalAndRefresh() {
    this.modalRef.hide();
  }
  
  createForm() {
    this.bookForm = this.formBuilder.group({
      'dateFrom': '',
      'dateTo': '',
      'hasGps': false,
      'hasWifi': false
    })
  }

  carToBook(car: Car) {
    this.bookedCar = car;
  }

  makeReservation(post) {
    const rental = {
      car_id: this.bookedCar.id,
      customer_id: this.getCustomerId(),
      status: 'reserved',
      dateFrom: post.dateFrom,
      dateTo: post.dateTo,
      hasGps: post.hasGps,
      hasWifi: post.hasWifi
    } as Rental;

    this.rentalService.add(rental).subscribe(rental => this.rentals.push(rental));
  }

  getCustomerId() {
    this.userLoggedIn = this.userService.getUserName();
    this.users = this.users.filter(user => user.userName === this.userLoggedIn);
    return this.users[0].customerId;
  }

  getUsers(): void {
    this.userService.getUsers().subscribe(users => this.users = users);
  }

  getRentals(): void {
    this.rentalService.getRentals().subscribe(rentals => this.rentals = rentals);
  }
}
