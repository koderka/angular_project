export class Car {
    id: number;
    mark: string;
    model: string;
    description: string;
    colour: string;
    productionDate: Date;
    retirementDate: Date;
    mileage: number;
    serviceStart: Date;
    serviceEnd: Date;
}